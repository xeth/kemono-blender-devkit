"""
Script to copy + clean base character models into mod resources.
Need to:
1. change texture names/sizes

TODO: more adjustments future
"""

import json
import os
from dataclasses import dataclass
import argparse

# parse first arg as full filepath of exported json file
parser = argparse.ArgumentParser()
parser.add_argument("path_in_json", help="path to input json file")
args = parser.parse_args()

path_in_json = args.path_in_json
filename = os.path.basename(path_in_json)
folder = os.path.dirname(path_in_json)

if not os.path.exists(path_in_json):
    print(f"ERROR: Path to input json does not exist: {path_in_json}")
    exit(1)

def find_local_mod_folder():
    path_mods = os.path.join(os.getenv("APPDATA"), "VintagestoryData", "Mods")
    if not os.path.exists(path_mods):
        raise Exception(f"ERROR: Path to mod folder does not exist: {path_mods}")
    # search for first mod folder that contains 'kemono.dll'
    for p in os.listdir(path_mods):
        if os.path.exists(os.path.join(path_mods, p, "kemono.dll")):
            return os.path.join(path_mods, p, "assets")
    raise Exception(f"ERROR: Could not find kemono mod folder in {path_mods}")

### for mod dev, check environment variable path
path_out_base = os.environ.get("VINTAGE_STORY_KEMONO_ASSETS") # .../kemono/assets/
# path_out_base = None # disable
if path_out_base is None:
    # search for local game mod folder:
    path_out_base = find_local_mod_folder()

if path_out_base is None or not os.path.exists(path_out_base):
    print(f"ERROR: Path to output does not exist: {path_out_base}")
    exit(1)
else:
    print(f"Using kemono asset path: {path_out_base}")

path_out_json = os.path.join(path_out_base, "kemono", "shapes", "entity", "kemono", "main", "kemono0.json")

# load model
with open(path_in_json, "r") as f:
    model = json.load(f)

# change textures and texture sizes
model["textures"] = {}
model["textureSizes"] = {}

def modify_element(element):
    """Recursively modify/adjust elements"""
    if "faces" in element:
        # disable faces with "disable" material
        for face in element["faces"].values():
            if "texture" in face and face["texture"] == "#disable":
                face["texture"] = "#null"
                face["enabled"] = False
    
    if "children" in element:
        for child in element["children"]:
            modify_element(child)

for element in model["elements"]:
    modify_element(element)

# write to output
with open(path_out_json, "w+") as f:
    print(f"Writing: {path_out_json}")
    json.dump(model, f)
    # json.dump(model, f, indent=2) # if debugging needed