"""
Script to clean + copy exported shape skin part json model file to mod folder.
"""

import json
import os
import argparse

# parse first arg as full filepath of exported json file
parser = argparse.ArgumentParser()
parser.add_argument("path_in_json", help="path to input json file")
args = parser.parse_args()

path_in_json = args.path_in_json
filename = os.path.basename(path_in_json)
folder = os.path.dirname(path_in_json)

if not os.path.exists(path_in_json):
    print(f"ERROR: Path to input json does not exist: {path_in_json}")
    exit(1)

# flag to create a model addon for the part
create_part_model_addon = False

def find_local_mod_folder():
    path_mods = os.path.join(os.getenv("APPDATA"), "VintagestoryData", "Mods")
    if not os.path.exists(path_mods):
        raise Exception(f"ERROR: Path to mod folder does not exist: {path_mods}")
    # search for first mod folder that contains 'kemono.dll'
    for p in os.listdir(path_mods):
        if os.path.exists(os.path.join(path_mods, p, "kemono.dll")):
            return os.path.join(path_mods, p, "assets")
    raise Exception(f"ERROR: Could not find kemono mod folder in {path_mods}")

### for mod dev, check environment variable path
path_out_base = os.environ.get("VINTAGE_STORY_KEMONO_ASSETS") # .../kemono/assets/
if path_out_base is None:
    # search for local game mod folder:
    path_out_base = find_local_mod_folder()
    create_part_model_addon = True

if path_out_base is None or not os.path.exists(path_out_base):
    print(f"ERROR: Path to output does not exist: {path_out_base}")
    exit(1)
else:
    print(f"Using kemono asset path: {path_out_base}")

model_type = None

if filename.startswith("headtattoo"):
    model_type = "headtattoo"
    path_out = os.path.join(path_out_base, "kemono", "shapes", "entity", "kemono", "skinparts", "headtattoo")
elif filename.startswith("head"):
    model_type = "head"
    path_out = os.path.join(path_out_base, "kemono", "shapes", "entity", "kemono", "skinparts", "head")
elif filename.startswith("hair"):
    model_type = "hair"
    path_out = os.path.join(path_out_base, "kemono", "shapes", "entity", "kemono", "skinparts", "hair")
elif filename.startswith("ear"):
    model_type = "ear"
    path_out = os.path.join(path_out_base, "kemono", "shapes", "entity", "kemono", "skinparts", "ear")
elif filename.startswith("eye"):
    model_type = "eye"
    path_out = os.path.join(path_out_base, "kemono", "shapes", "entity", "kemono", "skinparts", "eye")
elif filename.startswith("horn"):
    model_type = "horn"
    path_out = os.path.join(path_out_base, "kemono", "shapes", "entity", "kemono", "skinparts", "horn")
else:
    raise Exception(f"ERROR: Unsupported model type for file: {filename}")

path_out_json = os.path.join(path_out, filename)

# =============================================================================
# PROCESSING
# =============================================================================
with open(path_in_json, "r") as f:
    model = json.load(f)

# change textures and texture sizes
if model_type in {"hair"}:
    default_texture = "hair"
    model["textureWidth"] = 96
    model["textureHeight"] = 96
    model["textures"] = {}
    model["textureSizes"] = {
        "hair": [96, 96],
    }
elif model_type in {"headtattoo"}: # custom face emblem texture, 32x32
    default_texture = "headtattoo"
    model["textureWidth"] = 32
    model["textureHeight"] = 32
    model["textures"] = {}
    model["textureSizes"] = {
        "headtattoo": [32, 32],
    }
else:
    default_texture = "head"
    model["textureWidth"] = 64
    model["textureHeight"] = 64
    model["textures"] = {}
    model["textureSizes"] = {
        "head": [64, 64],
    }

# recursively walk elements, change texture name targets
def modify_element(elem, depth=0):
    # handle disabled face texture
    for face in elem["faces"].values():
        if face["texture"] == "#disable":
            face["texture"] = default_texture
            face["enabled"] = False
        elif face["texture"] == "#disable_hair":
            face["texture"] = "#hair"
            face["enabled"] = False
        elif face["texture"] == "#disable_headtattoo":
            face["texture"] = "#headtattoo"
            face["enabled"] = False
        
    for child in elem["children"]:
        modify_element(child, depth=depth+1)

# walk root elements
for element in model["elements"]:
    modify_element(element)

# write to output
with open(path_out_json, "w+") as f:
    json.dump(model, f, separators=(",", ":"))

# create part model addon so it can be detected ingame without
# needing to manually edit the model json files
if create_part_model_addon:
    # strip filename of .json extension and initial "modeltype-" prefix
    partcode = filename[filename.index("-")+1:-5]

    addon = {
        "addon": "kemono0",
        "skinparts": [
            {
                "code": model_type,
                "variants": [
                    {
                        "code": partcode,
                    }
                ]
            }
        ]
    }

    filename_addon = f"addon-{filename}"
    path_models = os.path.join(path_out_base, "assets", "kemono", "models")
    path_addon = os.path.join(path_models, filename_addon)
    with open(path_addon, "w+") as f:
        json.dump(addon, f, indent=2)
