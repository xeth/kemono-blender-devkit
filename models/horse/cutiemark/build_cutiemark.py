"""
Script to clean + copy horse cutiemark shape part json model files.
Need to:
1. change texture names to use emblem, change element texture
   name targets
2. add stepParentName to root element
"""

import argparse
import json
import os

# parse first arg as full filepath of exported json file
parser = argparse.ArgumentParser()
parser.add_argument("path_in_json", help="path to input json file")
args = parser.parse_args()

path_in_json = args.path_in_json
filename = os.path.basename(path_in_json)
folder = os.path.dirname(path_in_json)

if not os.path.exists(path_in_json):
    print(f"ERROR: Path to input json does not exist: {path_in_json}")
    exit(1)

# flag to create a model addon for the part
create_part_model_addon = False

def find_local_mod_folder():
    path_mods = os.path.join(os.getenv("APPDATA"), "VintagestoryData", "Mods")
    if not os.path.exists(path_mods):
        raise Exception(f"ERROR: Path to mod folder does not exist: {path_mods}")
    # search for first mod folder that contains 'kemono.dll'
    for p in os.listdir(path_mods):
        if os.path.exists(os.path.join(path_mods, p, "kemono.dll")):
            return os.path.join(path_mods, p, "assets")
    raise Exception(f"ERROR: Could not find kemono mod folder in {path_mods}")

### for mod dev, check environment variable path
path_out_base = os.environ.get("VINTAGE_STORY_KEMONO_ASSETS") # .../kemono/assets/
if path_out_base is None:
    # search for local game mod folder:
    path_out_base = find_local_mod_folder()
    create_part_model_addon = True

if path_out_base is None or not os.path.exists(path_out_base):
    print(f"ERROR: Path to output does not exist: {path_out_base}")
    exit(1)
else:
    print(f"Using kemono asset path: {path_out_base}")

model_type = "cutiemark"

path_out = os.path.join(path_out_base, "kemono", "shapes", "entity", "horse", "skinparts", "cutiemark")
path_out_json = os.path.join(path_out, filename)

with open(path_in_json, "r") as f:
    model = json.load(f)

# change textures and texture sizes
model["textures"] = {
    "cutiemark": "kemono:entity/common/emblem",
}
model["textureSizes"] = {
    "cutiemark": [32, 32],
}

# walk elements, change texture name targets
# (do not need recursive, these models will only be 1 level deep)
for element in model["elements"]:
    for face in element["faces"].values():
        face["texture"] = "#cutiemark"
        
        is_active_face = False
        for uv in face["uv"]: # coords [u0, v0, u1, v1]
            # active faces map whole uv, with coords (0, 0) to (32, 32)
            # invalid faces are mapped (0, 0) to (4, 4)
            # so if any face has a ~32 uv coord, its the active
            # while other faces need to be disabled
            if uv > 31:
                is_active_face = True
                break
        
        # disable unused faces
        if not is_active_face:
            face["enabled"] = False

# write to output
with open(path_out_json, "w+") as f:
    json.dump(model, f, indent=2)

# create part model addon so it can be detected ingame without
# needing to manually edit the model json files
if create_part_model_addon:
    # strip filename of .json extension and initial "modeltype-" prefix
    partcode = filename[filename.index("-")+1:-5]

    addon = {
        "addon": "horse0",
        "skinparts": [
            {
                "code": model_type,
                "variants": [
                    {
                        "code": partcode,
                    }
                ]
            }
        ]
    }

    filename_addon = f"addon-{filename}"
    path_models = os.path.join(path_out_base, "assets", "kemono", "models")
    path_addon = os.path.join(path_models, filename_addon)
    with open(path_addon, "w+") as f:
        json.dump(addon, f, indent=2)
