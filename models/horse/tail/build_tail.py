"""
Script to clean + copy exported shape skin part json model file to mod folder.
"""
import json
import os
import argparse

# parse first arg as full filepath of exported json file
parser = argparse.ArgumentParser()
parser.add_argument("path_in_json", help="path to input json file")
args = parser.parse_args()

path_in_json = args.path_in_json
filename = os.path.basename(path_in_json)
folder = os.path.dirname(path_in_json)

if not os.path.exists(path_in_json):
    print(f"ERROR: Path to input json does not exist: {path_in_json}")
    exit(1)

# flag to create a model addon for the part
create_part_model_addon = False

def find_local_mod_folder():
    path_mods = os.path.join(os.getenv("APPDATA"), "VintagestoryData", "Mods")
    if not os.path.exists(path_mods):
        raise Exception(f"ERROR: Path to mod folder does not exist: {path_mods}")
    # search for first mod folder that contains 'kemono.dll'
    for p in os.listdir(path_mods):
        if os.path.exists(os.path.join(path_mods, p, "kemono.dll")):
            return os.path.join(path_mods, p, "assets")
    raise Exception(f"ERROR: Could not find kemono mod folder in {path_mods}")

### for mod dev, check environment variable path
path_out_base = os.environ.get("VINTAGE_STORY_KEMONO_ASSETS") # .../kemono/assets/
if path_out_base is None:
    # search for local game mod folder:
    path_out_base = find_local_mod_folder()
    create_part_model_addon = True

if path_out_base is None or not os.path.exists(path_out_base):
    print(f"ERROR: Path to output does not exist: {path_out_base}")
    exit(1)
else:
    print(f"Using kemono asset path: {path_out_base}")

model_type = None

if filename.startswith("tailextra"):
    model_type = "tailextra"
    path_out = os.path.join(path_out_base, "kemono", "shapes", "entity", "horse", "skinparts", "tailextra")
elif filename.startswith("tail"):
    model_type = "tail"
    path_out = os.path.join(path_out_base, "kemono", "shapes", "entity", "horse", "skinparts", "tail")
else:
    raise Exception(f"ERROR: Unsupported model type for file: {filename}")

path_out_json = os.path.join(path_out, filename)

# =============================================================================
# PROCESSING
# =============================================================================
with open(path_in_json, "r") as f:
    model = json.load(f)

# change textures and texture sizes
if model_type == "tail":
    model["textures"] = {
        "tail": "kemono:entity/horse/tail/horse-tail",
    }
    model["textureWidth"] = 64
    model["textureHeight"] = 64
elif model_type == "tailextra":
    model["textures"] = {
        "tail": "kemono:entity/horse/tail/horse-tailextra",
    }
    model["textureWidth"] = 64
    model["textureHeight"] = 64
else:
    raise Exception(f"ERROR: Unknown model type: {model_type}")

# bone parent positions
# (copied from output of blender script that prints bone positions)
BONE_POSITIONS = {
    "b_Tail0": [4.176506042480469, 13.649999618530273, 0.0],
    "b_Tail1": [6.576509952545166, 13.649999618530273, 0.0],
    "b_Tail2": [9.076505661010742, 13.649999618530273, 0.0],
    "b_Tail3": [13.058438301086426, 13.649999618530273, 0.0],
    "b_Tail4": [17.456600189208984, 13.649999618530273, 0.0],
}

def modify_element(elem):
    """Recursively process element and children."""

    # modify positions to be relative to attach point (stepParent)
    parent_pos = BONE_POSITIONS[elem["stepParentName"]]
    elem["from"] = [
        elem["from"][0] - parent_pos[0],
        elem["from"][1] - parent_pos[1],
        elem["from"][2] - parent_pos[2],
    ]
    elem["to"] = [
        elem["to"][0] - parent_pos[0],
        elem["to"][1] - parent_pos[1],
        elem["to"][2] - parent_pos[2],
    ]
    elem["rotationOrigin"] = [
        elem["rotationOrigin"][0] - parent_pos[0],
        elem["rotationOrigin"][1] - parent_pos[1],
        elem["rotationOrigin"][2] - parent_pos[2],
    ]

    # handle disabled face texture
    for face in elem["faces"].values():
        if face["texture"] == "#disable":
            face["texture"] = "#tail"
            face["enabled"] = False
        elif face["texture"] == "#horse":
            face["texture"] = "#main"
    
    for child in elem["children"]:
        modify_element(child)

# walk root elements
for element in model["elements"]:
    modify_element(element)

# write to output
with open(path_out_json, "w+") as f:
    json.dump(model, f, indent=2)

# create part model addon so it can be detected ingame without
# needing to manually edit the model json files
if create_part_model_addon:
    # strip filename of .json extension and initial "modeltype-" prefix
    partcode = filename[filename.index("-")+1:-5]

    addon = {
        "addon": "horse0",
        "skinparts": [
            {
                "code": model_type,
                "variants": [
                    {
                        "code": partcode,
                    }
                ]
            }
        ]
    }

    filename_addon = f"addon-{filename}"
    path_models = os.path.join(path_out_base, "assets", "kemono", "models")
    path_addon = os.path.join(path_models, filename_addon)
    with open(path_addon, "w+") as f:
        json.dump(addon, f, indent=2)
