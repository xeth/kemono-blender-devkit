*DEPRECATED: new repo at:*
<https://gitlab.com/xeth/kemono/-/tree/master/blender?ref_type=heads>

![horsemod](images/horsewip4.jpg)

# vintagestory kemono mod blender dev kit

**mod link 4 alpha testers: <https://horsefucker.party/kemono/kemono_0.0.12.zip>**

dev kit for editing models/animations and adding additional 
skin parts shapes to vintagestory kemono mod models.

only tested on windows 10

(this dev kit will b eventually moved into main code repo)

videos:

<https://www.youtube.com/watch?v=TinUnjXu4s0>

<https://files.catbox.moe/ruhvow.mp4>

<https://files.catbox.moe/9y5nvc.mp4>

<https://files.catbox.moe/3zy5dn.mp4>

<https://files.catbox.moe/cna2q1.mp4>

<https://files.catbox.moe/uvfr73.mp4>


<video controls="true" allowfullscreen="true">
  <source src="https://files.catbox.moe/ruhvow.mp4" type="video/mp4">
</video>

<video controls="true" allowfullscreen="true">
  <source src="https://files.catbox.moe/9y5nvc.mp4" type="video/mp4">
</video>

<video controls="true" allowfullscreen="true">
  <source src="https://files.catbox.moe/3zy5dn.mp4" type="video/mp4">
</video>

<video controls="true" allowfullscreen="true">
  <source src="https://files.catbox.moe/cna2q1.mp4" type="video/mp4">
</video>

<video controls="true" allowfullscreen="true">
  <source src="https://files.catbox.moe/uvfr73.mp4" type="video/mp4">
</video>


## repository structure
```
images/                    - images for this guide
models/                    - blender files
  ├─ horse/
  |   ...skinparts...
  |   ├─ textures/         - horse textures
  |   ├─ build_horse.py    - post-export build script
  |   ├─ horse0.blend      - horse blender model
  |   └─ horse0.json       - exported horse vintagestory model
  |
  ├─ kemono/
  |   ...skinparts...
  |   ├─ textures/         - kemono textures
  |   ├─ build_horse.py    - post-export build script
  |   ├─ kemono0.blend     - kemono blender model
  |   └─ kemono0.json      - exported kemono vintagestory model
  |
  └─ seraph/               - seraph reference for animations
       └─ seraph0.blend
animations.md              - all animations needed
README.md                  - this guide
```


# vintagestory game + mod setup

## 1. download vintagestory **1.19-pre** version

![download vintagestory 1.19](images/vs_version.jpg)

this mod setup for **1.19 pre-release** versions:
<https://info.vintagestory.at/v1dot19>

and you may need net7 the C# runtime for the game, link is below,
and you can also find links in the account manager above:  
<https://dotnet.microsoft.com/en-us/download/dotnet/7.0>


## 2. download kemono mod beta version

**BUT DO NOT PUT IN YOUR MODS FOLDER, WE WANT TO UNZIP FIRST**

<https://horsefucker.party/kemono/kemono_0.0.12.zip>

(For alpha releases, see: <https://horsefucker.party>)


## 3. unzip the `kemono_0.0.X.zip` into your mods folder

the "X" is the mod version number

**manually unzip** the mod `kemono_0.0.X.zip` into a folder
named `kemono_0.0.X/` inside your Mods folder. if u do not know how
to find mods folder, u can open it from inside your game main menu
mods manager:

![in mod manager](images/vs_open_mods_folder.jpg)

this way we can directly modify + update the mod files when you export
from blender. note there will be two folders after you launch game,
your mods folder should have these:
```
kemono/            - this is where your saved skins .json are stored
kemono_0.0.X/      - unziped mod folder, check folder structure below
  ├─ assets/
  ├─ kemono.deps.json
  ├─ kemono.dll
  ├─ kemono.pdb
  ├─ modicon.png
  ├─ modinfo.json
  └─ protobuf-net.dll
```

you should see this in mod manager (the first grayed "kemono" thing
doesnt matter, "kemono (Code)" is the real mod):
![in mod manager](images/vs_kemono_installed.jpg)


### 3.1. custom assets folder (optional)

optional, if you are code modding or have kemono assets in a different
locaton, set  an environment variable on your system that points to the
assets path,
```
VINTAGE_STORY_KEMONO_ASSETS
```
set it to a custom path for assets, e.g. on windows i use something like
```
VINTAGE_STORY_KEMONO_ASSETS => D:/dev/vintagestory/kemono/resources/kemono/assets
```


## 4. test ingame (make a new creative world preferred)

if you launch a new world (for modelling, i recommend a creative world)
you should see the custom mod character customization screen below and
a random new skin,

- there are some mod built-in skin presets under the "Load Preset" button,
  you can select from dropdown then press "Load Preset"
- the "Load" and "Save" buttons on very top let you save your skin
  into personal local `.json` files stored in the `Mods/kemono/` folder
  (the other folder, NOT the unziped mod folder)

![char selection screen](images/horsemod_char_selection_screen.jpg)


## 5. cutiemark

the "Emblem" tab lets you paint your custom cutiemark ingame,
its a 32x32 pixel sized texture overlaid on top of the horse plots

![cutiemark painter screen](images/horsemod_cutiemark_painter.jpg)

## 6. finish your character, then setup blender below


# blender modeling setup

## 1. download blender
i'm currently using 3.3 LTS but shouldnt matter as long as it is a
blender 3 version, download link:

https://www.blender.org/download/


## 2. download blender vintagestory addon

download the blender addon as a **.zip** from:

https://github.com/phonon/blender-vintagestory-json

![download blender vintagestory addon](images/blender_vs_addon_download.jpg)


## 3. install blender vintagestory addon

open blender, in top left tabs on screen go to
**Edit > Preferences > Add-ons** to open the addons page.
Press the install button, find where you downloaded the addon **.zip**,
select the **.zip** file, then press **Install Add-on**.

![install blender vintagestory addon](images/blender_install_vs_addon.jpg)

after u install the addon, u should see the addon listed like below
(if not, search for it). make sure u press the checkbox shown below
to actually enable the addon.

![installed addon](images/blender_vs_installed.jpg)

i recommend u restart blender after

## 4. testing export

open `models/horse/horse0.blend`

![installed addon](images/blender_horse0.jpg)

let's test export. go to **File > Export > VintageStory (.json)**

![installed addon](images/blender_vs_export.jpg)

select the **horse0.json** in the blender file folder, make sure u are
overwriting and saving over this file. also make sure the blender file
is running the post export script. this is a python script in the folder
that will copy the exported model and automatically overwrite the json
file in the mod assets folder.

![installed addon](images/blender_horse_export.jpg)

to make sure it worked properly, check console:

![installed addon](images/blender_toggle_console.jpg)

make sure it says **Writing ....** without any **ERROR** messages
appearing (it should b obvious if error occured). if there are no errors
we can move on to editing model


## 5. adding/modifying animations guide:

we will mainly be working in the action editor dope sheet.

**this assumes you have basic knowledge on making blender bone
animations. if not, make sure u do basic blender bone animation
tutorials first.**

![blender action editor](images/blender_action_editor.jpg)

there are two ways we can add new animations


## 6. easy: copy animation action

easy way is to just copy the animation, rename it, and adjust keyframes
as needed:

![blender copy animation](images/blender_copy_animation.jpg)

reason this is easy is because animations need special vintagestory metadata
added using **Pose Markers**, these are the things at the bottom in the
dope sheet, the exporter needs these to set how the animations work ingame.
you can click on these, hover over them, then press **F2** to rename them.
to find what an animation needs, open the `models/seraph/seraph0.blend`
reference and see what animation there uses.

the **onAnimationEnd_[action]** pose marker is important, it actually
marks where the animation ends ingame. **it must be set on the frame
the animation should end on.**

![blender pose markers](images/blender_pose_markers.jpg)

also make sure the shield icon is pressed ("fake user"). this makes sure
blender still keeps the animation in the file when it is not selected.

![blender fake user](images/blender_animation_fake_user.jpg)

u can now start making/modifying the keyframes now


## 7. annoying: make new animation

annoying because u have to manually re-add pose markers

first remove the current animation (unlink action) with the X button:

![blender unlink action](images/blender_unlink_action.jpg)

make a new animation action:

![blender new animation](images/blender_animation_new.jpg)

the rest is same as above, but u need to manually go to 
**Marker > Add Marker** and setup the pose marker names:

![blender new animation](images/blender_new_animation_pose_marker_setup.jpg)


## 8. main guidelines on keyframes

- **only add keyframes on actually animated bones.
  avoid selecting everything and putting keyframe for all bones.**
  the game engine mixes together animations, having many keyframes
  on all parts makes mixing together animations more messy, for example
  if the holding lantern animation also had leg keyframes, this will mess
  up the model animation when it is holding lantern and walking

- **preferably only add rotation keyframes.
  only use location keyframe if absolutely necessary.**
  the game engine and blender animation system is different
  so mixed rotation + location keyframes will not look exactly the same.
  using only rotation keyframes will be closest matching.i

- **scale keyframes do not work.**

- **onAnimationEnd_[action] pose marker sets frame where animation ends.**
  reminder to set it on the frame where the animation ends ingame

![blender rotation keyframe](images/blender_keyframe_rotation_prefer.jpg)


## 9. hot-reloading animations ingame

after u worked on animation, export the file again.

after u export, the blender file runs a python script that automatically
updates the horse .json model in your mods folder. u can then type this
command ingame:
```
.skinreload
```

to reload animations **ingame (no need to restart!!!!11)**


## 10. use the `seraph0.blend` as a reference

u can use `models/seraph/seraph0.blend` as a reference to see what
animations looks like and what pose marker names should be:

![installed addon](images/blender_seraph0.jpg)


## 11. animations needed

check the `animations.md` file: [animations.md](animations.md)

here's google docs for who is animating / what is needed:  
https://docs.google.com/spreadsheets/d/1r1M2WL35H3zqiZaKaZn2haWZ0KkO3zgHjaDkHXG14IQ/edit?usp=sharing


## 12. skin parts editing (like hair, tails, horns, etc.)

**skin parts** are the selectable options for a model

there are two different types of skin parts:
- **texture skin parts**: e.g. kirin scales, zebra stripes
- **shape skin parts**: e.g. hair, tails, wings


## 12.1. texture parts

### 12.1.1. create your texture in wherever

only requirement is texture must be `.png` and same size as other
textures for this part type. e.g. if existing body overlay textures
are `128x128`, this must also be `128x128`.


### 12.1.2. save image as `part-name.png`

it must be in this format to be recognized by model.
e.g. for body overlays like zebra or kirin, the name must be
`bodyextra-kirin.png`. 


### 12.1.3. manually copy into mod folder

the folder path is in the kemono mod folder then
```
assets/kemono/textures/entity/[model]/[part]/part-name.png
```
then you must modify the model `.json` file located in
```
assets/kemono/models/[model].json
```
and add in the `part-name` under the correct "skin part" category.
currently no automated way of doing this, so you will need to copy manually
and modify the model json file.

then ingame run this command:
```
.skinreload textures
```
(if it does not work, try exit/rejoin the world)


## 12.2. shape parts

### 12.2.1. easy intialization: copy an existing part

![installed addon](images/skinpart/duplicate_existing.png)

i usually just copypasta an existing part as a base, this way you
get objects, UVs, materials, etc. already setup to work from.


### 12.2.2. model the skin part

main rules are:
- cuboids (resized/rotated cube) only
- **don't do any rotations in edit mode**. only use object mode rotations

if i need to stretch a cuboid, i usually work in normal rotation mode.
normally i scale in object mode along an axis. if i need to stretch a face,
i use edit mode, select face, and move along a normal axis.
im usually working in normal mode (hit "," hotkey to change modes),
since u can always move/rotate along a global XYZ axis by double pressing
the axis while transforming (e.g. in normal mode, if i want to move along
X global, i can grab `G` then double click `X` to move along X global axis):
![installed addon](images/skinpart/rotation_modes.png)


### 12.2.3. select skin part objects

![select skinpart objects](images/skinpart/select_skinpart_objects.png)
![selected skinpart objects](images/skinpart/selected_skinpart_objects.png)

for next parts, only the skin part (e.g. hair objects) should be selected
in the scene


### 12.2.4. apply scale: `Ctrl + A > Scale`
![apply scale](images/skinpart/apply_scale_to_selected.png)
![scale is 1](images/skinpart/scale_equals_1.png)

due to limitation in exporter, need to apply scale. select all objects
and apply scale, hotkeys are:
```
Ctrl + A > Scale
```
when scale is applied, all selected objects scale values should be 1


### 12.2.5. recalculate face normals
![recalculate face normals](images/skinpart/recalculate_face_normals.png)

sometimes when scaling with negative numbers or mirroring objects, the
face normals get flipped inside which causes textures to get messed up.

avoid this by always re-calculating face normals before exporting.
do this by entering edit mode, selecting all faces, then recalculating
face normals and unchecking inside.

hotkeys are:
```
Tab (Edit mode) > A (Select All) > Ctrl + Shift + N > Uncheck Inside

Tab again (Back to object mode)
```

make sure "inside" is unchecked when doing this.


### 12.2.6. assign `stepParentName`
![step parent name](images/skinpart/stepparentname.png)
![step parent name assign](images/skinpart/stepparentname_assign.png)

the **stepParentName** is the name of the shape vintage story will attach
the part to. its like setting the object's parent in blender.

open the N-Panel by pressing hotkey `N` and click on `VintageStory`
tab to open the vintage story utilities panel

this mod uses specific names for where things should be attached to.
e.g. for hair, type in `HeadCenter` and press enter to finish.

use following tables to decide which name to use:

**HORSE**:
| Horse Part  | stepParentName                        |
|-------------|---------------------------------------|
| Hair        | HeadCenter                            |
| Horn        | HeadCenter                            |
| Eyes        | HeadCenter                            |
| Ears        | HeadCenter                            |
|             |                                       |
| Tail        | b_Tail0                               |
| Tail        | b_Tail1                               |
| Tail        | b_Tail2                               |
| Tail        | b_Tail3                               |
| Tail        | b_Tail4                               |
|             |                                       |


**KEMONO**: TODO


### 12.2.7. save item as `part-name.json`

we can now export again: `File > Export > VintageStory (.json)`

![export part](images/skinpart/export_part.png)

the part name is important, it must be written in the format
```
[part]-name.json
```

the first chunk `[part]` is the category of part as it appears
in the ingame character editor. e.g. hair is called `hair`,
extra hair stuff like stripe is `hairextra`, tail is `tail` etc.
u can figure out the part name by looking at example part names in the dev
kit skin part folders.

use the following to reload skin parts:
```
.skinreload parts
```

if you edited a texture or added a custom texture, use
 ```
.skinreload parts textures
```

it should say skin parts are reloaded ingame:
![reload skin parts](images/skinpart/reload_parts_ingame.png)

*Note: if u want to verify it is outputting correctly, in your mod folder
there should be two new files created:*
```
Mods/kemono_0.0.X/assets/kemono/models/addon-[part]-name.json

Mods/kemono_0.0.X/assets/kemono/shapes/entity/horse/skinparts/[part]/[part]-name.json
```

the new part u added should appear at the bottom of the character part
selection list:
![reload skin parts](images/skinpart/new_skinpart_ingame.png)


### 12.2.8. quick checklist summary:
- **apply scale** (Click collection > Select Objects > Ctrl + A > Apply Scale)
- **recalculate normals** (Click collection > Select Objects > Tab to enter edit mode > A (to select all) > Ctrl + Shift + N > Uncheck inside)
- **selected objects you want to export**
- **objects must have `stepParentName` set**


### 12.2.9. hair part additional notes

for hair models here is additional setup notes:

- "hideme" object and its children are hidden when player wearing a helmet
- so for the hair parts you want hidden when wearing helmet,
**parent the parts to "hideme"**:
Select hair objects > Select "hideme" > `Ctrl + P` > Set parent to Object


# other modeling/animation rules

- **pls do not adjust/move/add any bones (in edit mode).**
  the current skeleton is designed for many specific ingame issues
  and works...the way parts attach to model also related to skeleton.
  editing bones may break other parts ingame.

- **pls do not edit/move any of the base model objects.**
  same reasons as above. by default i made dev kit have the base model
  objects selection be disabled. only edit if u rly rly rly know how
  vintagestory models + blender export work



# other random notes about model/animation issues
- horse bones used: 30/36 (max 36 animateable bones for vintagestory)
- if animations "twist" weirdly ingame, this is due to incompatibilities
  between blender and vintagestory animation systems...it cannot be
  easily fixed. just accept and cope about it :^(
- ears, mouth, eyes will eventually be split into separate
  configurable parts (soon TM)
- the `dummy_Name` objects create empty dummy objects ingame for
  positioning attachments (skin parts like horn, hair or items like armor)
- regular objects are prefixed like `MeshName` e.g. `MeshArmUpperR`
  to mark it as an object that is visually rendered
- bone objects ingame are automatically prefixed with `b_` after exporting,
  e.g. `ArmUpperR` bone will actually be named `b_ArmUpperR` ingame.
- CamelCase naming convention to match vintagestory convention



# other mod ingame commands

## .skinsave [name]
```
.skinsave [name]
```
this saves your current skin to a json file located into your
`Mods/kemono/` folder (you can use this to share skins)

## .skinload [name]
```
.skinload [name]
```
this loads a skin from a json skin file from your `Mods/kemono/`
folder

## .skinreload [parts] [textures]
```
.skinreload parts
.skinreload textures
.skinreload parts textures
```
the `.skinreload` can also reload skin part shapes and textures together
by putting in those arguments above

## .emblemsave [name]
```
.emblemsave [name]
```
this saves your current emblem (cutie mark) to a 32x32 png file located into
your `Mods/kemono/` folder (you can use this to share cutie mark)

## .emblemload [name]
```
.emblemload [name]
```
this loads a 32x32 png from from your `Mods/kemono/` folder, must be 32x32
currently does not support auto resizing images


# thanks 4 helping !!111

u can request skin parts from me & i willl also add a built in skin preset
config for u as credits :3333

## credits/contact/seethe
- horse, kemono model designed/rig: `@xeth9000`
- horse animations: tenklop

seraph reference model imported from vintagestory tyron model
